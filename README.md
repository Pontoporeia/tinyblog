# Tinyblog

Fork of Petabyt's [TinyBlog](https://github.com/petabyt/tinyblog)


Single File HTML/CSS/PHP blog in less than 100 lines.
See it in action: https://tgm.happyngreen.fr/tinyblog/

## Features

    Tiny source code. Just pull index.php and customize.
    Minimal markdown parser. Easy to customize to your liking.

## Setup

In the posts folder, create a file named "1" for the first
post, "2" for the second, and so on.

For optimal File and Folder Permissions and Ownership, set all folders and subfolders to `755` and files to `644`. Use Filezilla to apply recursively.

## Theme

The current theme is based on the original theme in `archive/index-daniel.php`. 
Current improvements include [lib/highlight.js](https://github.com/highlightjs/highlight.js) to highlight code syntax in `pre code` blocks, as well an overhaul in colors, positioning, etc.

## Markdown parsing

My fork uses [lib/parsedown.php](https://github.com/erusev/parsedown). It supports most of the typical
Markdown syntax, but has some additional features provided by the original custom Tinyblog markdown parser when appropriate:

    Type --- to insert a "Read More" link, and cut off the rest of the text.
    Use \* to prevent the asterisk from being recognized.
