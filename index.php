<?php
    include("lib/Parsedown.php");
    // ini_set('display_errors', 1);
    // ini_set('display_startup_errors', 1);
    // error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="eng">
<head>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>TGM_RP</title>
	<link rel="stylesheet" href="style.css">
	<link rel="icon" type="image/png" href="assets/logo.png">
</head>
<body>
<a class="home" href="../../index.php" ><img class="home" src="assets/logo.png" alt=""></a>

	<div class="header">
		<a class="rss" rel="alternate" href="rss.php"><img class="home" src="assets/rss.png" alt=""></a>
		<h1><a href="index.php">TGM Space</a></h1>
		<h3>Space to think out "write" about subjects that interest me</h3>
		
	</div>
	<?php
		# TODO: changing the naming convention to dates and time
		# Make sure number is valid
		if (isset($_GET["post"])) {
			if (!is_numeric($_GET["post"])) {
				echo("Invalid post");
				die();
			}
		}
	
		# Automatically count files in /posts
		$postsDirectory = scandir("posts");
		$postCount = count($postsDirectory) - 2;

		# Show specific post or all
		if (!isset($_GET["post"])) {
			for ($post = $postCount; $post >= 1; $post--) {
				makePost($post, FALSE);
			}
		} else {
			if (file_exists("posts/" . strval($_GET["post"]))) {
				makePost($_GET["post"], TRUE);
			} else {
				echo("Bad param");
			}
		}

		function makePost($post, $showRest) {
			$post_body = file_get_contents("posts/" . strval($post));
			$post_body = parse($post_body, $showRest, $post);

			echo("<div class='post' title='Post number $post'>" . $post_body . "</div>");
		}
		
		function parse($string, $showRest, $post) {
			$Parsedown = new Parsedown();
			$asHtml = $string;
			
			# Replace --- with mothing and add "back" link or add "read more" if chosen.
			if ($showRest == TRUE) {
				$asHtml = preg_replace("/---/s", "", $asHtml);
				$asHtml .= "<a class='min' href='index.php'>Back</a>";
			} else {
				$asHtml = preg_replace("/---(.+)/s", "<p><a class='min' href='?post=$post'>Read more</a></p>", $asHtml);
			}
			
			$asHtml = $Parsedown->text($asHtml);

			return $asHtml;
		}
	?>
	
</body>
	<link rel="stylesheet" href="lib/qtcreator-dark.min.css">
	<script src="lib/highlight.min.js"></script>
	<script>hljs.highlightAll();</script>
</html>
