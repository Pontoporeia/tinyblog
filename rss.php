<?php header("Content-Type: application/xml");
echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<rss version="2.0"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:wfw="http://wellformedweb.org/CommentAPI/"
  xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns:atom="http://www.w3.org/2005/Atom"
  xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
  xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
>
<channel>
  <title>TGM Space</title>
  <link><strong>https://tgm.happyngreen.fr/tinyblog/</strong></link>
  <description>Space to think out "write" about subjects that interest me</description>
  <language>fr-FR</language>
  <sy:updatePeriod>hourly</sy:updatePeriod>
  <sy:updateFrequency>1</sy:updateFrequency>
  <generator>https://tgm.happyngreen.fr/blog/</generator>
  <?php
		# Automatically count files in /posts
	$postsDirectory = scandir("posts");
	$postCount = count($postsDirectory) - 2;
    $page=isset($_GET["page"])?$_GET["page"]:0;
    $max=8; //Set the number of RSS posts to be displayed
    for ($post = $postCount; $post >= 1; $post--) {
      $txt=file("posts/" . strval($post));
	  $txt[0]=$txt[0] ."@";
	  $txt[1]=$txt[1] ."@";
	  $txt[2]=$txt[2] . "" . @$txt[3] . "" .@$txt[4] ."@";
	  $txt[3]=strval($post);
      $array[]= $txt[0]. "" .$txt[1]. "" .$txt[2]. "" .$txt[3];
  }
  
  $i=count($array); //get array array Number of all article descriptions Paging

  for ($j=$max*$page;$j<($max*$page+$max)&&$j<$i;++$j) { //Cyclic conditions control the number of images displayed

      $post = explode('@', $array[$j]);   // Splitting an array @ before depositing it for easy reading

      //.$post[0].  Title  .$post[3]. URL-id   .$post[1]. date .$post[3]. Description

      $content = <<<HTML


    <item>
      <title>$post[0]</title>
      <link>https://tgm.happyngreen.fr/tinyblog/?post=$post[3]</link>
      <guid>https://tgm.happyngreen.fr/tinyblog/?post=$post[3]</guid>
      <dc:creator>tpagm</dc:creator>
      <pubDate>$post[1]</pubDate>
      <description><![CDATA[$post[2]]]></description>
      <content:encoded><![CDATA[$post[2]]]></content:encoded>
    </item>

HTML;

      echo $content;
  }
		
	?>
</channel>
</rss>